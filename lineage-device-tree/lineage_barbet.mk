#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from barbet device
$(call inherit-product, device/google/barbet/device.mk)

PRODUCT_DEVICE := barbet
PRODUCT_NAME := lineage_barbet
PRODUCT_BRAND := google
PRODUCT_MODEL := Pixel 5a
PRODUCT_MANUFACTURER := google

PRODUCT_GMS_CLIENTID_BASE := android-google

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="barbet-user Tiramisu TPB1.220310.029.A1 8480170 release-keys"

BUILD_FINGERPRINT := google/barbet/barbet:Tiramisu/TPB1.220310.029.A1/8480170:user/release-keys
